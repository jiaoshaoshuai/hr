package edu.taru.office.view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import edu.taru.office.pojo.User;
import edu.taru.office.service.UserServiceImpl;
//修改密码

public class PasswordView extends JFrame {
	private JPasswordField passwordField_old;
	private JPasswordField passwordField_new;
	//String str = LoginView.list.get(0);
	
public PasswordView() {
	//System.out.println(str);
	getContentPane().setLayout(null);
	
	JLabel label_old = new JLabel("\u539F\u5BC6\u7801");
	label_old.setBounds(0, 32, 54, 15);
	getContentPane().add(label_old);
	
	JLabel label_new = new JLabel("\u65B0\u5BC6\u7801");
	label_new.setBounds(0, 90, 54, 15);
	getContentPane().add(label_new);
	
	passwordField_old = new JPasswordField();
	passwordField_old.setText(LoginView.nowuser.getPassword());//把旧密码放入文本框
	passwordField_old.setBounds(64, 29, 168, 31);
	getContentPane().add(passwordField_old);
	
	
	passwordField_new = new JPasswordField();
	passwordField_new.setBounds(62, 87, 170, 31);
	getContentPane().add(passwordField_new);
	/*passwordField_new.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			
		}
		
	});*/
	
	JButton button = new JButton("\u786E\u8BA4");
	button.setBounds(80, 169, 93, 23);
	getContentPane().add(button);
	button.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
//修改当前用户
			
			String newpass=new String(passwordField_new.getPassword());
			System.out.println(newpass);
			User user1=new User();
			user1.setPassword(newpass);
			UserServiceImpl UserServiceImpl=new UserServiceImpl();
			UserServiceImpl.updatepassword(newpass,user1.getId());
			//String passwordNew=new String(passwordField_new.getPassword());
			JOptionPane.showMessageDialog(PasswordView.this, "修改成功！");
			
			PasswordView.this.dispose();
		}
		
	});
	
	
	
	
	
	this.setTitle("修改密码");
	this.setSize(300, 300);
	this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	this.setVisible(true);
	this.setLocationRelativeTo(null);
}
}
