package edu.taru.office.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import edu.taru.office.pojo.User;
import edu.taru.office.service.LoginServiceImpl;
import edu.taru.office.service.ManagementSystem;
import edu.taru.office.until.SqlHelp;

import javax.swing.ImageIcon;
//登录界面
public class LoginView extends JFrame{
	JButton loginBtn_D=new JButton("登陆");
	JButton loginBtn_T=new JButton("退出");
	JTextField loginF_YHM=new JTextField();
	JPasswordField loginPassText=new JPasswordField();
	JLabel loginLable_YHM=new JLabel("用户名");
	JLabel loginLable_MA=new JLabel("密码");
	//public static List<String> list = new ArrayList<String>();
   public 	static User nowuser=null;
	public LoginView() {
		//设置布局  用户名
		getContentPane().setLayout(null);//绝对定位布局
		loginLable_YHM.setBounds(20, 200, 70, 35);
		getContentPane().add(loginLable_YHM);
		//密码
		loginLable_MA.setBounds(20, 250, 70, 35);
		getContentPane().add(loginLable_MA);
		//用户名文本框
		loginF_YHM.setBounds(90, 200, 180, 35);
		getContentPane().add(loginF_YHM);
		//密码文本框
		loginPassText.setBounds(90, 250, 180, 35);
		getContentPane().add(loginPassText);
		//登录按钮框
		loginBtn_D.setBounds(300, 200, 70, 35);
		getContentPane().add(loginBtn_D);
		//退出按钮框
		loginBtn_T.setBounds(300, 250, 70, 35);
		getContentPane().add(loginBtn_T);
		//导入图片
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(LoginView.class.getResource("/edu/taru/office/base/login.jpg")));
		label.setBounds(39, 0, 345, 190);
		getContentPane().add(label);
		//退出按钮事件监听器
		loginBtn_T.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
			
		});
		//登录按钮事件监听器
		loginBtn_D.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {

				String username=loginF_YHM.getText();
				String password=new String(loginPassText.getPassword());
				LoginServiceImpl LoginServiceImpl=new LoginServiceImpl();
				try{
					User  user=LoginServiceImpl.login(username, password);
				
					/*System.out.println(username);
					System.out.println(	password);*/
				
					//list.add(password);
					if(user==null) {
						JOptionPane.showMessageDialog(LoginView.this, "用户或密码错误！");
					}else {
						nowuser =user;
						MainView mianview=new MainView();
						LoginView.this.dispose();
						
					}	
				}catch(Exception e){
					JOptionPane.showMessageDialog(LoginView.this, "登录异常！");
				}
				
			
			}
			
		});
		
		this.setTitle("人力资源管理系统");
		this.setSize(400, 400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
	}
	public static void main(String[] args) {
		LoginView loginView=new LoginView();
	}
}
