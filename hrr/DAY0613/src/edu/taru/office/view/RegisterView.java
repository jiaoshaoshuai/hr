package edu.taru.office.view;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;


import edu.taru.office.pojo.User;


import edu.taru.office.service.UserServiceImpl;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterView extends JFrame {
	public JTextField textField;
	public JPasswordField passwordField_1;
	public JTextField textField_1;
	public JTextField textField_2;
	public JTextField textField_3;
	public RegisterView() {
		getContentPane().setLayout(null);
		//用户名
		JLabel label = new JLabel("\u7528\u6237\u540D");
		label.setBounds(32, 112, 54, 15);
		getContentPane().add(label);
		//密码
		JLabel label_1 = new JLabel("\u5BC6\u7801");
		label_1.setBounds(32, 154, 54, 15);
		getContentPane().add(label_1);
		//性别
		JLabel label_2 = new JLabel("\u6027\u522B");
		label_2.setBounds(32, 268, 54, 15);
		getContentPane().add(label_2);
		//籍贯
		JLabel label_3 = new JLabel("\u7C4D\u8D2F");
		label_3.setBounds(32, 314, 54, 15);
		getContentPane().add(label_3);
		
		//用户名文本框
		textField = new JTextField();
		textField.setBounds(118, 109, 224, 21);
		getContentPane().add(textField);
		textField.setColumns(10);
		//备注按钮
		JLabel lblNewLabel = new JLabel("\u5907\u6CE8");
		lblNewLabel.setBounds(32, 372, 54, 15);
		getContentPane().add(lblNewLabel);
		//密码文本框
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(118, 151, 224, 21);
		getContentPane().add(passwordField_1);
		//男
		final JRadioButton rdbtnNewRadioButton = new JRadioButton("\u7537");
		rdbtnNewRadioButton.setBounds(127, 264, 64, 23);
		getContentPane().add(rdbtnNewRadioButton);
		//女
		final JRadioButton radioButton = new JRadioButton("\u5973");
		radioButton.setBounds(210, 264, 70, 23);
		getContentPane().add(radioButton);
		//逻辑组建
		ButtonGroup ds=new ButtonGroup();
		ds.add(rdbtnNewRadioButton);
		ds.add(radioButton);
		
		//省份
		final String []citys={"陕西省","新疆维吾尔自治区"};
		final JComboBox comboBox = new JComboBox(citys);
		comboBox.setBounds(118, 323, 224, 21);
		getContentPane().add(comboBox);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(118, 378, 224, 24);
		getContentPane().add(textArea);
		
		
		//重置按钮
		JButton button_1 = new JButton("\u91CD\u7F6E");
		button_1.setBounds(288, 429, 93, 23);
		getContentPane().add(button_1);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(null);
				passwordField_1.setText(null);
				textArea.setText(null);
				radioButton.isSelected();

			}
		});
		//确定按钮添加事件监听器，添加一个注册监听事件
		JButton button = new JButton("\u786E\u5B9A");
		button.setBounds(118, 429, 93, 23);
		getContentPane().add(button);
		
		final JLabel lblId = new JLabel("ID");
		lblId.setBounds(32, 10, 54, 15);
		getContentPane().add(lblId);
		
		textField_1 = new JTextField();
		textField_1.setBounds(118, 7, 224, 30);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel label_4 = new JLabel("\u59D3\u540D");
		label_4.setBounds(32, 60, 54, 15);
		getContentPane().add(label_4);
		
		textField_2 = new JTextField();//姓名
		textField_2.setBounds(118, 57, 224, 24);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JLabel label_5 = new JLabel("\u7535\u8BDD");
		label_5.setBounds(32, 193, 54, 15);
		getContentPane().add(label_5);
		
		textField_3 = new JTextField();//电话
		textField_3.setBounds(118, 190, 224, 30);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String id=textField_1.getText();
				String name=textField_2.getText();//姓名
				String username=textField.getText();//用户名
				String password=new String(passwordField_1.getPassword());//密码
				String telephone=textField_3.getText();//电话

/*				String sex="男";
				if(radioButton.isSelected()){
					sex="女";
				}
				//选择城市
				int index=comboBox.getSelectedIndex();
				String city=citys[index];*/
				User user=new User();
				user.setId(id);
				user.setName(name);
				user.setUsername(username);;
				user.setPassword(password);
				user.setTelephone(telephone);
				try {
					UserServiceImpl userServiceImpl=new UserServiceImpl();
					userServiceImpl.register(user);
					JOptionPane.showMessageDialog(RegisterView.this, "注册成功！");
					RegisterView.this.dispose();
				} catch (Exception e2) {
					JOptionPane.showMessageDialog(RegisterView.this, "注册失败！");
				}
				MainView mainView=new MainView();
			
			}
		});

		
		this.setTitle("人力资源管理系统");
		this.setSize(600, 500);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
			
	}
}
