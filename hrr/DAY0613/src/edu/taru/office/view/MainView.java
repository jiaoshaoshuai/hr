package edu.taru.office.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import edu.taru.office.service.ManagementSystem;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.ImageIcon;
//主界面
public class MainView extends JFrame{
	private JTable table;
	private JTable table_1;
	public MainView() {
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		//权限管理
		JMenu menu = new JMenu("\u6743\u9650\u7BA1\u7406");
		menuBar.add(menu);
		//用户信息
		JMenuItem mntmNewMenuItem = new JMenuItem("\u7528\u6237\u4FE1\u606F");
		menu.add(mntmNewMenuItem);
		//修改密码
		JMenuItem menuItem = new JMenuItem("\u4FEE\u6539\u5BC6\u7801");
		menu.add(menuItem);
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PasswordView passwordView=new PasswordView();
			}
			
		});
		
		JSeparator separator = new JSeparator();
		menu.add(separator);
		//退出
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("\u9000\u51FA");
		//添加退出监听事件
		menu.add(mntmNewMenuItem_1);
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});
		//用户管理
		JMenu menu_1 = new JMenu("\u7528\u6237\u7BA1\u7406");
		menuBar.add(menu_1);
		//注册用户
		JMenuItem menuItem_2 = new JMenuItem("注册用户");
		menu_1.add(menuItem_2);
		//查询用户
		JMenuItem menuItem_3 = new JMenuItem("查询用户");
		menu_1.add(menuItem_3);
			
		menuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserView userview=new UserView();
				MainView.this.dispose();
			}
		});
		
		//添加注册用户事件
		menuItem_2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterView registerview=new RegisterView();
				MainView.this.dispose();
			}
		});
		//雇员管理
		JMenu menu_2 = new JMenu("\u96C7\u5458\u7BA1\u7406");
		menuBar.add(menu_2);
		//员工信息
		JMenuItem menuItem_1 = new JMenuItem("员工信息");
		menu_2.add(menuItem_1);
		
		//奖惩管理
		JMenu menu_3 = new JMenu("\u5956\u60E9\u7BA1\u7406");
		menuBar.add(menu_3);
		//奖励
		JMenuItem menuItem_4 = new JMenuItem("\u5956\u52B1");
		menu_3.add(menuItem_4);
		menuItem_4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AwardView award=new AwardView();
			}
			
		} );
		//惩罚
		JMenuItem menuItem_5 = new JMenuItem("\u60E9\u7F5A");
		menu_3.add(menuItem_5);
		
		//薪资管理
		JMenu menu_4 = new JMenu("\u85AA\u8D44\u7BA1\u7406");
		menuBar.add(menu_4);
		
	/*	JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(MainView.class.getResource("/edu/taru/office/image/2.jpg")));
		getContentPane().add(lblNewLabel, BorderLayout.CENTER);
		*/
		table = new JTable();
		getContentPane().add(table, BorderLayout.WEST);
		
		JLabel lblNewLabel_1 = new JLabel("");
		getContentPane().add(lblNewLabel_1, BorderLayout.SOUTH);
		
		table_1 = new JTable();
		getContentPane().add(table_1, BorderLayout.NORTH);
		//添加员工信息监听事件
		menuItem_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ManagementSystem mangagementSystem=new ManagementSystem();
				mangagementSystem.select();
				
			}
		});
	
		this.setTitle("人力资源管理系统");
		this.setSize(600, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		
	}
}
