package edu.taru.office.view;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.FlowLayout;

import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTabbedPane;

import java.awt.BorderLayout;

import javax.swing.JTable;
import javax.swing.UIManager;

import edu.taru.office.pojo.User;
import edu.taru.office.service.ManagementSystem;
import edu.taru.office.service.UserServiceImpl;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Vector;

public class UserView extends JFrame {
	private JTextField textField;
	private JTable table;
	private JTextField textField_1;
	public UserView() {
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "\u641C\u7D22\u9762\u677F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 0, 469, 108);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u7F16\u53F7");
		lblNewLabel.setBounds(45, 30, 54, 15);
		panel.add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(245, 30, 2, 2);
		panel.add(scrollPane);
		
		textField = new JTextField();//按照编号查询
		textField.setBounds(120, 27, 115, 21);
		panel.add(textField);
		textField.setColumns(10);
		
		
		
		final String []strs={"ID","姓名","用户名","密码","电话"};
		String[][]data=new String[5][5];
		table = new JTable(data,strs);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "\u6570\u636E\u5217\u8868", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 233, 469, 194);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_1.add(scrollPane_1, BorderLayout.CENTER);
		
		scrollPane_1.setViewportView(table);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "\u63A7\u5236\u9762\u677F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 120, 469, 103);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		
		
		JButton button_1 = new JButton("\u5237\u65B0");//刷新
		button_1.setBounds(10, 38, 93, 23);
		panel_2.add(button_1);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//ManagementSystem mangagementsystem=new ManagementSystem();
				UserServiceImpl userServiceImpl=new UserServiceImpl();
				try {
					Vector<Vector<String>> vectors=	userServiceImpl.select();
					//Vector<Vector<String>> vectors=mangagementsystem.select();
					//头
					Vector	<String>vector=new Vector<String>();	
					for(int i=0;i<strs.length;i++) {
						vector.add(strs[i]);
					}
					DefaultTableModel tablemodel = new DefaultTableModel(vectors,vector);
					table.setModel(tablemodel);
					table.updateUI();
				} catch (Exception e2) {
					System.out.println("弹框");
				}	
				
			}
		});
		
		JButton button_2 = new JButton("\u6DFB\u52A0\u7528\u6237");//添加用户
		button_2.setBounds(113, 38, 93, 23);
		panel_2.add(button_2);
		button_2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				RegisterView register=new RegisterView();
			}
			
		});
	/**
	 *	修改：1：数据回显 
	 */
		JButton button_3 = new JButton("\u4FEE\u6539\u7528\u6237");//修改
		button_3.setBounds(231, 38, 93, 23);
		panel_2.add(button_3);
		button_3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				String id=table.getValueAt(row,0).toString();
				String name=table.getValueAt(row,1).toString();
				String username=table.getValueAt(row,2).toString();
				String password=table.getValueAt(row,3).toString();
				String telephone=table.getValueAt(row,4).toString();
				
				
				User user=new User();
				user.setId(id);
				user.setName(name);
				user.setUsername(username);
				user.setPassword(password);
		     	user.setTelephone(telephone);
				RegisterView registerView=new RegisterView();
				registerView.textField_1.setText(user.getId());
				registerView.textField_2.setText(name);
				registerView.textField.setText(username);
				registerView.passwordField_1.setText(password);
				registerView.textField_3.setText(telephone);
				UserServiceImpl  userServiceImpl=new UserServiceImpl();
				userServiceImpl.update(user);
				
				}
			
		});
		
		JButton button_4 = new JButton("\u5220\u9664\u7528\u6237");//删除
		button_4.setBounds(346, 38, 93, 23);
		panel_2.add(button_4);
		button_4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				int row = table.getSelectedRow();
				String id=table.getValueAt(row, 0).toString();
				UserServiceImpl UserServiceImpl=new UserServiceImpl();
				if(id!=null) {
					UserServiceImpl.delete(id);
					JOptionPane.showMessageDialog(UserView.this, "删除成功！");
					}	
				}catch(Exception ex){
					JOptionPane.showMessageDialog(UserView.this, "删除失败！");
				}
		}		
		});
		
		textField_1 = new JTextField();
		textField_1.setBounds(241, 71, 83, 21);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
	
		
		JButton button = new JButton("\u641C\u7D22");//搜索
		button.setBounds(286, 26, 93, 23);
		panel.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserServiceImpl UserServiceImpl =new UserServiceImpl();
				String message=textField.getText();
				/*String ID=textField.getText();
				String Name=textField.getText();
				String Sex=textField.getText();
				String City=textField.getText();
			ManagementSystem mangagementsystem=new ManagementSystem();*/
			try {
				Vector<Vector<String>> vectors=UserServiceImpl.likeSearch(message);
				//头
				Vector	<String>vector=new Vector<String>();
				for(int i=0;i<strs.length;i++) {
					vector.add(strs[i]);
				}
				DefaultTableModel tablemodel = new DefaultTableModel(vectors,vector);
				table.setModel(tablemodel);
				table.updateUI();
			} catch (Exception e2) {
				System.out.println("弹框");
			}	
			}
			
		} );
		
		this.setTitle("信息查询");
		this.setSize(600, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
	
	}
	
		

}
