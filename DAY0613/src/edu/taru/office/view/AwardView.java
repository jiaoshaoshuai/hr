package edu.taru.office.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.BorderLayout;

public class AwardView extends JFrame {
	private JTable table;
public AwardView() {
	getContentPane().setLayout(null);
	
	JPanel panel = new JPanel();
	panel.setBorder(new TitledBorder(null, "\u5956\u60E9", TitledBorder.LEADING, TitledBorder.TOP, null, null));
	panel.setBounds(10, 34, 364, 185);
	getContentPane().add(panel);
	panel.setLayout(new BorderLayout(0, 0));
	
	JScrollPane scrollPane = new JScrollPane();
	panel.add(scrollPane, BorderLayout.CENTER);
	
	final String []strs={"奖惩项目","奖惩类型","金额","奖惩时间"};
	String[][]data=new String[5][5];
	table = new JTable(data,strs);
	scrollPane.setViewportView(table);


	
	this.setTitle("奖惩管理");
	this.setSize(400, 300);
	this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	this.setVisible(true);
	this.setLocationRelativeTo(null);
	
}
}
