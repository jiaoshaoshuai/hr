


package edu.taru.office.until;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;



/**
 * 数据库操作的工具类：
 * （1：sql语句不同 2：参数不同 3：查询和	dml不同）
 * @author k35
 * @param <T>
 *
 */
public class SqlHelp<T> {
	private  static Properties properties=new Properties();
	//static Connection coon=null;//全局唯一 线程不安全
	static ThreadLocal<Connection>  local=new ThreadLocal<Connection>();
	
	//读取
	static{
		try {
			properties.load(SqlHelp.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
//1：加载驱动（一次  写在静态块）
	{
		try {
			Class.forName(properties.getProperty("driver"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
/**
 * 打开链接
 */
	public static Connection openConnection(){
		Connection coon=local.get();
		try {
			if(coon==null||coon.isClosed()){
				coon=DriverManager.getConnection(properties.getProperty("url"), properties.getProperty("user"),properties.getProperty("password"));
				local.set(coon);
			}			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return coon;
	}
	/**
	 * 查询
	 */
		public static <T> List<T>   select2(String sql,RowHandlerMapper<T> handlerMapper,Object ... params){
			Connection coon=SqlHelp.openConnection();
			ResultSet rs=null;//结果集代表从数据查询的游标
			PreparedStatement pst =null;
			List <T> rows=null;
			try {
		        pst =coon.prepareStatement(sql);
				if(params!=null){
					for(int i=0;i<params.length;i++){
						pst.setObject(i+1, params[i]);
					}
				}
			rs=	pst.executeQuery();//返回结果集
			rows=handlerMapper.maping(rs);
			} catch (SQLException e) {
				e.printStackTrace();
				throw new RuntimeException("查询时异常",e);
			}finally{
				if(rs!=null){
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			if(pst!=null){
				try {
					pst.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			return rows;
		}
		
	
/**
 * 查询
 */
	public static  ResultSet select(String sql,Object ... params){
		Connection coon=SqlHelp.openConnection();
		ResultSet rs=null;//结果集代表从数据查询的游标
		try {
			PreparedStatement pst =coon.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
		rs=	pst.executeQuery();//返回结果集
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询时异常",e);
		}
		return rs;
	}
/**
 * Hashmap查询
 * @param <T>
 * @param sql
 * @param handlerMapper
 * @param params
 * @return
 */
	public static List <HashMap<String,Object>>   select3(String sql,Object ... params){
		Connection coon=SqlHelp.openConnection();
		ResultSet rs=null;//结果集代表从数据查询的游标
		PreparedStatement pst =null;
		List <HashMap<String,Object>> rows=new ArrayList<HashMap<String,Object>>();
		try {
	        pst =coon.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
		rs=	pst.executeQuery();//返回结果集
		ResultSetMetaData rsmd= rs.getMetaData();//获取列头元数据
		int length=rsmd.getColumnCount();//获取列数
		while(rs.next()){
			HashMap<String,Object> map=new HashMap<String,Object>();
			for(int i=0;i<length;i++){
				String columnLable=	rsmd.getColumnLabel(i+1);
				map.put(columnLable, rs.getObject(columnLable));
			}
			rows.add(map);
		}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("查询时异常",e);
		}finally{
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException("查询时异常",e);
				}
			}
		}

		if(pst!=null){
			try {
				pst.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException("查询时异常",e);
			}
		}
	
		return rows;
	}
	
/**
 * 更新(删除 修改 更新)
 * @return 
 */
	public static int update(String sql,Object ... params){
		int row=0;
		Connection coon=SqlHelp.openConnection();
		PreparedStatement pst =null;
		try {
			pst=coon.prepareStatement(sql);
			if(params!=null){
				for(int i=0;i<params.length;i++){
					pst.setObject(i+1, params[i]);
				}
			}
		row=	pst.executeUpdate();//返回结果集
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("更新时异常",e);
		}
		return row;
	
	}
/**
 * 关闭链接
 */
	public static  void close(){
		Connection coon=local.get();
		if(coon!=null){
			try {
				coon.close();
				coon=null;
				local.remove();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static interface RowHandlerMapper<T>{
		public List<T> maping(ResultSet rs);
	}
	
	
}



